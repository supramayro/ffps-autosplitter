state("Pizzeria Simulator") {
  bool loading : "mmfs2.dll", 0x6FBCC;
}

startup {
  vars.savePath = System.IO.Path.Combine(Environment.GetEnvironmentVariable("appdata"), @"MMFApplications\FNAF6");
  vars.fileExists = System.IO.File.Exists(vars.savePath);
  refreshRate = 30;
}

init {
  vars.phase = 0;
}

split {
  var split = false;
  if(vars.fileExists) {
    var lines = System.IO.File.ReadAllLines(vars.savePath);
    var currPhase = 0;
    foreach(var line in lines) {
      if(line.StartsWith("phase")) {
        Int32.TryParse(line.Substring(line.Length-1, 1), out currPhase);
        if(currPhase == 3 && vars.phase == 1) {
          split = true;
          break;
        }
      }
    }
    vars.phase = currPhase;
  }
  return split;
}

isLoading {
  return current.loading;
}
