This is an autosplitter for Freddy Fazbear's Pizzeria Simulator for LiveSplit. For features other than pausing on loads, it reads from the save file that updates immediately upon important actions and events.

Features:
- Pauses on load
- Splits when clicking Log Off

Planned Features:
- Detecting Bankruptcy and Insanity endings
- Detecting Lorekeeper certificate